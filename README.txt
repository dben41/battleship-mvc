BattleShip MVC
Daryl Bennett
11/05/2014
==============
+   NOTES    +
==============

-Finished games allow you to keep firing missles, but they announce 
the winner after each missle is fired.
-There is no "Current Game" screen. This idea is encapsulated in the sidebar.
 The current game is clearly highlighted, and the stats (who's turn, game
 state, missle count) are updated in real time.
-I get strange results with the emulator. When I start out horizontal
and flip the screen to landscape, it creates two instances of the game.
I must flip it first and then run it. (This problem doesn't appen on my phone.)
-I didn't have time to implement my persistence strategies. I started on GSON
 and I wrote a SQLite Schema to store the game state and load it back up again.


runningGames(rid, String uuid, int currentTurn, int gridId, int gamePhase) 
grid(gridId, content) #content is either hit, miss, ocean, boat