package net.darylbennett.battleship;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.UUID;

/**
 * Created by Daryl on 11/3/2014.
 */
public class GameFragment extends Fragment {
    GameModel currentGame;
    TestGridView playerOneView;
    TestGridView playerTwoView;
    TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /***********************create model***********************************/
        //MenuModel bb = MenuModel.getInstance();
        currentGame = new GameModel(); //this shouldn't be called, this should be created from instance?

        MenuModel bb = MenuModel.getInstance();
        playerOneView = new TestGridView(getActivity());
        playerTwoView = new TestGridView(getActivity());
        textView = new TextView(getActivity());
        textView.setText("Welcome to Battleship! Please choose a game on the left.");
        textView.setBackgroundColor(Color.rgb(161, 161, 161));

        LinearLayout vertical = new LinearLayout(getActivity());
        vertical.setOrientation(LinearLayout.VERTICAL);

        LinearLayout test = new LinearLayout(getActivity());
        test.setOrientation(LinearLayout.HORIZONTAL);

        test.addView(playerOneView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.FILL_PARENT, 5));
        test.addView(playerTwoView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 5));

        vertical.addView(test, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.FILL_PARENT, 8));
        vertical.addView(textView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 2));

        //player1.
        return vertical;
    }

    public void setGameDetailIdentifier(UUID identifier) {
        if (playerOneView == null)
            return;
        // TODO: get getGameDetails which could create a grid
        //get the game identifiers
        RestGameModel currentGame = MenuModel.getInstance().getGame(identifier);

        //currentGame = gameResourceIdentifier;
        //update grids
        playerOneView.grid = currentGame.playerGrid;
        playerTwoView.grid = currentGame.opponentGrid;
        playerOneView.invalidate();
        playerTwoView.invalidate();

        /***********************create view******************************/




    }
}


