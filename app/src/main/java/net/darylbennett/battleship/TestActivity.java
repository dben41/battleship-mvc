package net.darylbennett.battleship;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;


public class TestActivity extends Activity {
    GameModel currentGame;
    TestGridView playerOneView;
    TestGridView playerTwoView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /***********************create model***********************************/
        currentGame = new GameModel();

        currentGame.setModelChangedListener(new GameModel.ModelChangedListener() {
            @Override
            public void modelChanged(Point.Status[][] grid) {
                //needs to take in a player to only update one grid at a time
                if(currentGame.currentTurn == GameModel.Player.PLAYER1) {
                    playerOneView.updateViews(grid);
                    playerOneView.invalidate();
                }
                if(currentGame.currentTurn == GameModel.Player.PLAYER2) {
                    playerTwoView.updateViews(grid);
                    playerTwoView.invalidate();
                }
            }
        });

        /***********************create view******************************/
        playerOneView  = new TestGridView(this);
        playerTwoView  = new TestGridView(this);

        //passes in the boats
        playerOneView.setGrid(currentGame.player1Grid);
        playerTwoView.setGrid(currentGame.player2Grid);

        playerTwoView.setOnGridTouchListener(new TestGridView.OnGridTouchListener() {
            @Override
            public void onGridTouch(int x, int y) {
                if(currentGame.currentTurn == GameModel.Player.PLAYER2)
                    return;
                currentGame.launchMissle(currentGame.currentTurn, new Point(x,y));
                playerTwoView.cloakGrid=false;
                playerOneView.cloakGrid=true;
                popupWindow();
                playerOneView.invalidate();
                playerTwoView.invalidate();

            }
        });

        playerOneView.setOnGridTouchListener(new TestGridView.OnGridTouchListener() {
            @Override
            public void onGridTouch(int x, int y) {
                if(currentGame.currentTurn == GameModel.Player.PLAYER1)
                    return;

                currentGame.launchMissle(currentGame.currentTurn, new Point(x,y));
                playerTwoView.cloakGrid=true;
                playerOneView.cloakGrid=false;
                popupWindow();
                playerOneView.invalidate();
                playerTwoView.invalidate();

            }
        });

        //player one can't see his oponent
        playerTwoView.cloakGrid=true;
        playerOneView.cloakGrid=false;

        LinearLayout test = new LinearLayout(this);
        test.setOrientation(LinearLayout.HORIZONTAL);

        test.addView(playerOneView ,new LinearLayout.LayoutParams( ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.FILL_PARENT, 5));
        test.addView(playerTwoView,new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 5));


        //player1.
        setContentView(test);
    }

    public void popupWindow()
    {
        //findViewById(R.id.gameLayout).setVisibility(View.INVISIBLE);
        playerOneView.setVisibility(View.INVISIBLE);
        playerTwoView.setVisibility(View.INVISIBLE);

        // Show popup dialog box
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Confirmation")
                .setMessage("Player " + currentGame.oppositeTurn() + " has fired! Please pass your Anroid Device to Player " + currentGame.currentTurn )
                .setNeutralButton("Ready", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        playerOneView.setVisibility(View.VISIBLE);
                        playerTwoView.setVisibility(View.VISIBLE);
                    }

                })
                .show();
    }
}
