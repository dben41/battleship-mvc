package net.darylbennett.battleship;

import java.util.ArrayList;

/**
 * Represents a boat drawn on the screen
 * Created by Daryl on 11/1/2014.
 */
public class Boat {
    ArrayList<Point> boatCoordinates;
    GameModel.Player player;
    Boolean isSunk;

    public Boat( )
    {
        boatCoordinates = new ArrayList<Point>();
        isSunk = false;
    }

    public void addPoint(Point p)
    {
        boatCoordinates.add(p);
    }

    public int size()
    {
        return boatCoordinates.size();
    }

    public void setPlayer(GameModel.Player i)
    {
        player = i;
    }

    public void setIsSunk(Boolean b)
    {
        isSunk = b;
    }

    public ArrayList<Point> getPoints()
    {
        return boatCoordinates;
    }


}
