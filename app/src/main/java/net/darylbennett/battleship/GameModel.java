package net.darylbennett.battleship;

import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

/**
 * This class represents a Game and its current state.
 * It is meant to be a modular model as found in the MVC pattern
 * Created by Daryl on 11/1/2014.
 */
public class GameModel {
    //declare class vars
    ArrayList<Boat> shipsOnMap;
    Player currentTurn;
    Point.Status[][] player1Grid;
    Point.Status[][] player2Grid;
    ArrayList<Point> launchedMissles;
    public UUID identifier;
    Player winner;
    Phase gamePhase;
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    String name;

    public GameModel()
    {
        player1Grid = new Point.Status[10][10];
        player2Grid = new Point.Status[10][10];
        MenuModel b = MenuModel.getInstance();
        launchedMissles = new ArrayList<Point>();
        gamePhase = Phase.STARTING;
        currentTurn = Player.PLAYER1;
        shipsOnMap = new ArrayList<Boat>();
        //shipsOnMap = randomShipGenerator();
        randomShipGenerator();
        instantiateGrids();
    }

    private void instantiateGrids() {
        for(int i = 0; i < 9; i++)
            for(int j = 0; j < 9; j++)
            {
                player1Grid[i][j] = Point.Status.OCEAN;
                player2Grid[i][j] = Point.Status.OCEAN;
            }
        for(Boat b: getPositionOfBoats(Player.PLAYER1))
        {
            for(Point p: b.getPoints())
            {
                player1Grid[p.x][p.y] = Point.Status.SHIP;
            }
        }
        for(Boat b: getPositionOfBoats(Player.PLAYER2))
        {
            for(Point p: b.getPoints())
            {
                player2Grid[p.x][p.y] = Point.Status.SHIP;
            }
        }
//        modelChangedListener.modelChanged(player1Grid);
//        modelChangedListener.modelChanged(player2Grid);
    }

    public enum Phase {
        STARTING,
        IN_PROGRESS,
        P1_WON,
        P2_WON;
    }

    public enum Player {
        PLAYER1,
        PLAYER2
    }

    /**
     * Boat objects contain all the coordinates of a boat and also their owners
     * @return
     */
    private void randomShipGenerator()
    {
        //Create player 1 ships
        //size 2
        Boat temp = new Boat();
        temp.setPlayer(Player.PLAYER1);
        //set points for boat
       // int random = (int)(Math.random()*8);

        //create 5 random ints
        ArrayList<Integer> randomInts = new ArrayList<Integer>();
        ArrayList<Integer> randomInts2 = new ArrayList<Integer>();
        while(randomInts.size()<5)
        {
            //this controls the x, we don't want things on the same column
            int tempInt = (int)(Math.random()*9);
            if(!randomInts.contains(tempInt))
            {
                randomInts.add(tempInt);
                randomInts2.add((int)(Math.random()*5));
            }
            //this controls the y

        }
        temp.addPoint(new Point(randomInts.get(0),randomInts2.get(0)));
        temp.addPoint(new Point(randomInts.get(0),randomInts2.get(0)+1));
        //add to collection
        shipsOnMap.add(temp);
        //reset
        temp = new Boat();

        //size 3
        temp.setPlayer(Player.PLAYER1);
        //set points for boat
        temp.addPoint(new Point(randomInts.get(1),randomInts2.get(1)));
        temp.addPoint(new Point(randomInts.get(1),randomInts2.get(1)+1));
        temp.addPoint(new Point(randomInts.get(1),randomInts2.get(1)+2));
        //add to collection
        shipsOnMap.add(temp);
        //reset
        temp = new Boat();

        //size 3
        temp.setPlayer(Player.PLAYER1);
        //set points for boat
        temp.addPoint(new Point(randomInts.get(2),randomInts2.get(2)));
        temp.addPoint(new Point(randomInts.get(2),randomInts2.get(2)+1));
        temp.addPoint(new Point(randomInts.get(2),randomInts2.get(2)+2));
        //add to collection
        shipsOnMap.add(temp);
        //reset
        temp = new Boat();

        //size 4
        temp.setPlayer(Player.PLAYER1);
        //set points for boat
        temp.addPoint(new Point(randomInts.get(3),randomInts2.get(3)));
        temp.addPoint(new Point(randomInts.get(3),randomInts2.get(3)+1));
        temp.addPoint(new Point(randomInts.get(3),randomInts2.get(3)+2));
        temp.addPoint(new Point(randomInts.get(3),randomInts2.get(3)+3));
        //add to collection
        shipsOnMap.add(temp);
        //reset
        temp = new Boat();

        //size 5
        temp.setPlayer(Player.PLAYER1);
        //set points for boat
        temp.addPoint(new Point(randomInts.get(4),randomInts2.get(4)));
        temp.addPoint(new Point(randomInts.get(4),randomInts2.get(4)+1));
        temp.addPoint(new Point(randomInts.get(4),randomInts2.get(4)+2));
        temp.addPoint(new Point(randomInts.get(4),randomInts2.get(4)+3));
        temp.addPoint(new Point(randomInts.get(4),randomInts2.get(4)+4));
        //add to collection
        shipsOnMap.add(temp);


        //reset
        randomInts = new ArrayList<Integer>();
        randomInts2 = new ArrayList<Integer>();
        while(randomInts.size()<5)
        {
            //this controls the x, we don't want things on the same column
            int tempInt = (int)(Math.random()*9);
            if(!randomInts.contains(tempInt))
            {
                randomInts.add(tempInt);
                randomInts2.add((int)(Math.random()*5));
            }
            //this controls the y
        }
        temp = new Boat();

        //create player 2 ships
        temp.setPlayer(Player.PLAYER2);
        //set points for boat
        temp.addPoint(new Point(randomInts.get(0),randomInts2.get(0)));
        temp.addPoint(new Point(randomInts.get(0),randomInts2.get(0)+1));
        //add to collection
        shipsOnMap.add(temp);
        //reset
        temp = new Boat();

        //size 3
        temp.setPlayer(Player.PLAYER2);
        //set points for boat
        temp.addPoint(new Point(randomInts.get(1),randomInts2.get(1)));
        temp.addPoint(new Point(randomInts.get(1),randomInts2.get(1)+1));
        temp.addPoint(new Point(randomInts.get(1),randomInts2.get(1)+2));
        //add to collection
        shipsOnMap.add(temp);
        //reset
        temp = new Boat();

        //size 3
        temp.setPlayer(Player.PLAYER2);
        //set points for boat
        temp.addPoint(new Point(randomInts.get(2),randomInts2.get(2)));
        temp.addPoint(new Point(randomInts.get(2),randomInts2.get(2)+1));
        temp.addPoint(new Point(randomInts.get(2),randomInts2.get(2)+2));
        //add to collection
        shipsOnMap.add(temp);
        //reset
        temp = new Boat();

        //size 4
        temp.setPlayer(Player.PLAYER2);
        //set points for boat
        temp.addPoint(new Point(randomInts.get(3),randomInts2.get(3)));
        temp.addPoint(new Point(randomInts.get(3),randomInts2.get(3)+1));
        temp.addPoint(new Point(randomInts.get(3),randomInts2.get(3)+2));
        temp.addPoint(new Point(randomInts.get(3),randomInts2.get(3)+3));
        //add to collection
        shipsOnMap.add(temp);
        //reset
        temp = new Boat();

        //size 5
        temp.setPlayer(Player.PLAYER2);
        //set points for boat
        temp.addPoint(new Point(randomInts.get(4),randomInts2.get(4)));
        temp.addPoint(new Point(randomInts.get(4),randomInts2.get(4)+1));
        temp.addPoint(new Point(randomInts.get(4),randomInts2.get(4)+2));
        temp.addPoint(new Point(randomInts.get(4),randomInts2.get(4)+3));
        temp.addPoint(new Point(randomInts.get(4),randomInts2.get(4)+4));
        //add to collection
        shipsOnMap.add(temp);
        //reset
        temp = new Boat();
    }

    /**
     * @return the current player
    */
    public Player whosTurn()
    {
        return currentTurn;
    }

    public Player oppositeTurn(){
        if(currentTurn == Player.PLAYER1)
            return Player.PLAYER2;
        else
            return Player.PLAYER1;
    }

    public Player isGameOver()
    {
        //ArrayList<Point> hits = new ArrayList<Point>();
        int i = 0;
        for(Point p : getLaunchedMissles(Player.PLAYER1))
        {
            if(p.status == Point.Status.HIT)
                i++;
        }
        if(i >= 17)
            return Player.PLAYER1;
        i = 0;
        for(Point p : getLaunchedMissles(Player.PLAYER2))
        {
            if(p.status == Point.Status.HIT)
                i++;
        }
        if(i >= 17)
            return Player.PLAYER2;

        return null;
    }

    /**
     * Gets all the launched missles
     */
    public ArrayList<Point> getLaunchedMissles(Player currentPlayer) {
        ArrayList<Point> temp = new ArrayList<Point>();
        for (Point p : launchedMissles) {
            if (p.player == currentPlayer)
                temp.add(p);
        }

        return temp;
    }

    public ArrayList<Boat> getPositionOfBoats(Player currentPlayer)
    {
        ArrayList<Boat> temp = new ArrayList<Boat>();
        for(Boat b: shipsOnMap)
            if(b.player == currentPlayer)
                temp.add(b);
        return temp;
    }

    public Phase getGamePhase()
    {
        return gamePhase;
    }


    public Boolean launchMissle(Player currentPlayer, Point p)
    {
        if(gamePhase == Phase.STARTING)
            gamePhase = Phase.IN_PROGRESS;
        if(currentTurn == Player.PLAYER1)
        {
            currentTurn = Player.PLAYER2;
            Log.i("MissleLaunched", "Current Player: " + currentTurn);
        }
        else {
            currentTurn = Player.PLAYER1;
            Log.i("MissleLaunched", "Current Player: " + currentTurn);
        }
        try {
            //error check
            for (Point launchedMissle : getLaunchedMissles(currentPlayer)) {
                if (launchedMissle.x == p.x && launchedMissle.y == p.y)
                    throw new Exception("Someone already shot to that point!");
            }
            //assign the point the player who fired the missle
            p.player = currentPlayer;
            //assign the status as a hit or miss
            if (hit(p)) {
                p.status = Point.Status.HIT;
                //add to the collection
                launchedMissles.add(p);
                if (currentPlayer == Player.PLAYER1) {
                    player2Grid[p.x][p.y] = p.status;
                    modelChangedListener.modelChanged(player2Grid);
                }
                else {
                    player1Grid[p.x][p.y] = p.status;
                    modelChangedListener.modelChanged(player1Grid);
                }
                return true;
            } else {
                p.status = Point.Status.MISS;
                launchedMissles.add(p);
                if (currentPlayer == Player.PLAYER1) {
                    player2Grid[p.x][p.y] = p.status;
                    modelChangedListener.modelChanged(player2Grid);
                }
                else {
                    player1Grid[p.x][p.y] = p.status;
                    modelChangedListener.modelChanged(player1Grid);
                }
            }

            return false;
        } catch (Exception e){
            e.printStackTrace();
            return false;
    }
    }

    /**
     * Indicates if a missle has hit an opponents boat.
     * @param p
     * @return
     */
    public Boolean hit(Point p)
    {
        Boolean h = false;
        for(Boat b : getPositionOfBoats(p.player)){
            for(Point boatPoint: b.getPoints())
            {
                if(boatPoint.x == p.x && boatPoint.y == p.y)
                {
                    h = true;
                    break;
                }
            }
        }

        return h;
    }


    /* Listener Stuff*/
    //lets the controller know if grid has changed:
    //updates the grid
    public interface ModelChangedListener{
        public void modelChanged(Point.Status[][] grid);
    }
    ModelChangedListener modelChangedListener = null;
    public void setModelChangedListener(ModelChangedListener listener){
        modelChangedListener = listener;
    }


}

