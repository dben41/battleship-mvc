package net.darylbennett.battleship;

/**
 * Represens x,y coordinates
 * Created by Daryl on 11/1/2014.
 */
public class Point {
    public int x;
    public int y;
    //used for launching missles
    public GameModel.Player player;
    public Status status;

    public Point(int xx, int yy){
        x = xx;
        y = yy;
    }
    public Point(){}
    //overloaded constructor


    public enum Status {
        OCEAN,
        SHIP,
        HIT,
        MISS;
    }

}
