package net.darylbennett.battleship;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This contains a set of methods that interface with a RESTful web service
 * Created by Daryl on 11/13/2014.
 */
public class RestInterface {

    public static final String BASE_URL = "http://battleship.pixio.com/api/games";
    public enum GameStatus{
        DONE,
        WAITING,
        PLAYING;
    }
    public class NewGameInfo
    {
        public String playerId;
        public String gameId;
    }

    public class GameListItem{
        String id;
        String name;
        GameStatus status;
    }
    public class GameDetails{
        String id;
        String name;
        String player1;
        String player2;
        String winner;
        int missilesLaunched;
    }

    public class PlayerId{
        String playerId;
    }

    public class GuessResult{
         boolean hit;
        int shipSunk;
    }

    public class WhoseTurnIsItObject{
        boolean isYourTurn;
        String winner;
    }

    public enum boardState{
        NONE,
        MISS,
        HIT,
        SHIP;
    }

    public class BoardSquare{
        int xPos;
        int yPos;
        boardState status;
    }

    public class Board{
        BoardSquare[] playerBoard;
        BoardSquare[] opponentBoard;
    }




    //
    //*Gets all the details associated with game.
    //*a
    //* Example:
    //* http://battleship.pixio.com/api/games/79e9c35f-2158-4b37-8c7a-48c377835c4e
    //* {
    //"id": "79e9c35f-2158-4b37-8c7a-48c377835c4e",
    //"name": "A9",
    //"player1": "thatoneguy",
    //"player2": "myname",
    //"winner": "IN PROGRESS"
    //}
    //
    public static GameDetails getGameDetails(String id){
        try {
            //connect
            HttpClient client =  new DefaultHttpClient();
            HttpGet    get   = new HttpGet(BASE_URL + "/" + id);

            //execute
            HttpResponse response = client.execute(get);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error check
            if (responseString == null)
                return  null;

            //convert JSON string into an object
            Gson gson = new Gson();
            Type type = new TypeToken<GameDetails>() {}.getType();
            GameDetails gameDetails = gson.fromJson(responseString, type);
            return gameDetails;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    /*
     *Gets the list of current games off the server.
     *Converts the JSON data into something the MenuModel can understand
     * http://battleship.pixio.com/api/games
     */
    public static GameListItem[] getGameList(){
        GameListItem[] listItems = null;
        try {
            //connect
            HttpClient client =  new DefaultHttpClient();
            HttpGet    get   = new HttpGet(BASE_URL);

            //execute
            HttpResponse response = client.execute(get);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error check
            if (responseString == null)
                return  null;

            //convert JSON string into an object
            Gson gson = new Gson();
            Type type = new TypeToken<GameListItem[]>() {}.getType();
            listItems = gson.fromJson(responseString, type);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return listItems;

    }

    public static String joinGame(String playerName, String id){
        try {
            //connect
            HttpClient client =  new DefaultHttpClient();
            HttpPost post = new HttpPost(BASE_URL + "/" + id + "/join");

            // Request parameters and other properties.
            List<NameValuePair> params = new ArrayList<NameValuePair>(1);
            params.add(new BasicNameValuePair("playerName", playerName));
            post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

            //execute
            HttpResponse response = client.execute(post);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error check
            if (responseString == null)
                return  null;

            //convert JSON string into an object
            Gson gson = new Gson();

            Type type = new TypeToken<PlayerId>() {}.getType();
            PlayerId playerId = gson.fromJson(responseString, type);
            //game couldn't be joined if null
            if(playerId.playerId==null)
                return "Game could not be joined.";
            return playerId.playerId;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static NewGameInfo createGame(String gameName, String playerName){
        NewGameInfo gameInfo = null;
        try {
            //connect
            HttpClient client =  new DefaultHttpClient();
            HttpPost post = new HttpPost(BASE_URL);

            // Request parameters and other properties.
            List<NameValuePair> params = new ArrayList<NameValuePair>(2);
            params.add(new BasicNameValuePair("gameName", gameName));
            params.add(new BasicNameValuePair("playerName", playerName));
            post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

            //execute
            HttpResponse response = client.execute(post);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error check
            if (responseString == null)
                return  null;

            //convert JSON string into an object
            Gson gson = new Gson();
            Type type = new TypeToken<NewGameInfo>() {}.getType();
            gameInfo = gson.fromJson(responseString, type);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return gameInfo;
    }
    public static GuessResult fireMissle(String playerId, int xPos, int yPos, String gameId){
        try {
            //connect
            HttpClient client =  new DefaultHttpClient();
            HttpPost post = new HttpPost(BASE_URL + "/" + gameId + "/guess");

            // Request parameters and other properties.
            List<NameValuePair> params = new ArrayList<NameValuePair>(3);
            params.add(new BasicNameValuePair("playerId", playerId));
            params.add(new BasicNameValuePair("xPos", Integer.toString(xPos)));
            params.add(new BasicNameValuePair("yPos", Integer.toString(yPos)));
            post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

            //execute
            HttpResponse response = client.execute(post);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error check
            if (responseString == null)
                return  null;

            //convert JSON string into an object
            Gson gson = new Gson();
            Type type = new TypeToken<GuessResult>() {}.getType();
            GuessResult gameInfo = gson.fromJson(responseString, type);
            return gameInfo;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static WhoseTurnIsItObject whosTurn(String gameId, String playerId){
        try {
            //connect
            HttpClient client =  new DefaultHttpClient();
            HttpPost post = new HttpPost(BASE_URL + "/" + gameId + "/status");

            // Request parameters and other properties.
            List<NameValuePair> params = new ArrayList<NameValuePair>(1);
            params.add(new BasicNameValuePair("playerId", playerId));
            post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

            //execute
            HttpResponse response = client.execute(post);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error check
            if (responseString == null)
                return  null;

            //convert JSON string into an object
            Gson gson = new Gson();
            Type type = new TypeToken<WhoseTurnIsItObject>() {}.getType();
            WhoseTurnIsItObject gameInfo = gson.fromJson(responseString, type);
            return gameInfo;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static Board getBoard(String gameId, String playerId){

        try {
            //connect
            HttpClient client =  new DefaultHttpClient();
            HttpPost post = new HttpPost(BASE_URL + "/" + gameId + "/board");

            // Request parameters and other properties.
            List<NameValuePair> params = new ArrayList<NameValuePair>(1);
            params.add(new BasicNameValuePair("playerId", playerId));
            post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

            //execute
            HttpResponse response = client.execute(post);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error check
            if (responseString == null)
                return  null;

            //convert JSON string into an object
            Gson gson = new Gson();
            Type type = new TypeToken<Board>() {}.getType();
            Board board = gson.fromJson(responseString, type);
            return board;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
