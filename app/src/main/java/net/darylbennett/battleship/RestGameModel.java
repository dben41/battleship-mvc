package net.darylbennett.battleship;

import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Daryl on 11/16/2014.
 */
public class RestGameModel {
    RestInterface.GameStatus gameStatus;
    String gameName;
    String gameId;
    String playerName;
    String playerId;
    int misslesLaunched;
    //field to keep track of boats sunk: TODO
    String turn;
    Point.Status[][] playerGrid;
    Point.Status[][] opponentGrid;
    public RestGameModel()
    {}












    /* Listener Stuff*/
    //lets the controller know if grid has changed:
    //updates the grid
    public interface ModelChangedListener{
        public void modelChanged(Point.Status[][] grid);
    }
    ModelChangedListener modelChangedListener = null;
    public void setModelChangedListener(ModelChangedListener listener){
        modelChangedListener = listener;
    }

}
