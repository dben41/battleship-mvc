package net.darylbennett.battleship;

import android.app.Fragment;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Set;
import java.util.UUID;

/**
 * Displays all the running games
 * Created by Daryl on 11/3/2014.
 */
public class GameListFragment extends Fragment implements ListAdapter {
    UUID[] gameIdentifiersByName = null;
    ListView gameListView;

    public UUID getCurrentGame() {
        return currentGame;
    }

    public void setCurrentGame(UUID currentGame) {
        this.currentGame = currentGame;
    }

    UUID currentGame;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout vertical = new LinearLayout(getActivity());
        vertical.setOrientation((LinearLayout.VERTICAL));
        gameListView = new ListView(getActivity());
        gameListView.setAdapter(this);

        gameListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //UUID gameIdentifier = gameIdentifiersByName[position]; //TODO: is this the right way
                UUID gameIdentifier = (UUID)MenuModel.getInstance().getIdentifiers().toArray()[(int)getItemId(position)];
                if(onGameSelectedListener != null) {
                    onGameSelectedListener.onGameSelected(GameListFragment.this, gameIdentifier);
                }
            }
        });

        //add button
        Button addButton = new Button(getActivity());
        addButton.setText("Add");
        vertical.addView(addButton);
        vertical.addView(gameListView);

        //button logic
        addButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //TODO: add listener to talk to MainActivity
                //newGameListener.addNewGameEvent();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                          //this adds a new game
                            newGameListener.addNewGameEvent();
//                       // MenuModel.getInstance().addGame(test1);
//                        // getCount();

                        gameListView.invalidateViews();
                    }
                });
            }
        });


        return vertical;
    }

    //listener stuff
    public interface OnGameSelectedListener{
        public void onGameSelected(GameListFragment gameListFragment, UUID identifier);
    }
    public OnGameSelectedListener getOnGameSelectedListener(){ return onGameSelectedListener;}
    public void setOnGameSelectedListener(OnGameSelectedListener onGameSelectedListener) {
        this.onGameSelectedListener = onGameSelectedListener;
    }
    OnGameSelectedListener onGameSelectedListener = null;

    public interface NewGameListener{
        public void addNewGameEvent();
    }
    public NewGameListener getNewGameListener(){return newGameListener;}
    NewGameListener newGameListener = null;
    public void setNewGameListener(NewGameListener listener){
        this.newGameListener = listener;
    }
    //end listener stuff


    @Override
    public boolean isEmpty() {
        return getCount()<=0;
    }

    @Override
    public int getCount() {
        if(gameIdentifiersByName == null)
        {
            Set<UUID> gameIdentifiers = MenuModel.getInstance().getIdentifiers();
            //TODO: Order list
            gameIdentifiersByName = gameIdentifiers.toArray(new UUID[gameIdentifiers.size()]);
        }
        Set<UUID> gameIdentifiers = MenuModel.getInstance().getIdentifiers();
        //TODO: Order list
        gameIdentifiersByName = gameIdentifiers.toArray(new UUID[gameIdentifiers.size()]);
        return MenuModel.getInstance().getIdentifiers().size();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public Object getItem(int position) {
        return gameIdentifiersByName[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int i = this.getCount();

        //MenuModel bb = MenuModel.getInstance();
        UUID gameIdentifier = gameIdentifiersByName[(int)getItemId(position)];

       RestGameModel game = MenuModel.getInstance().getGame(gameIdentifier);

        LinearLayout root = new LinearLayout(getActivity());
        root.setOrientation(LinearLayout.VERTICAL);


        TextView gameTitleView = new TextView(getActivity());
        gameTitleView.setText(game.gameName); //TODO: change


        //TextView playersTurn = new TextView(getActivity());
        //playersTurn.setText(game.currentTurn +"'s turn."); //TODO: change

        TextView titleView = new TextView(getActivity());
        titleView.setText(game.gameStatus.toString()); //TODO: change

        TextView gameTitleView2 = new TextView(getActivity());

        gameTitleView2.setText("Missles: " + game.misslesLaunched); //TODO: change

        if(gameIdentifier == this.getCurrentGame())
            root.setBackgroundColor(Color.CYAN);
        root.addView(titleView);
        root.addView(gameTitleView);
        //root.addView(playersTurn);
        root.addView(gameTitleView2);

        return root;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {}
    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {}






}
