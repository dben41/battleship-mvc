package net.darylbennett.battleship;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * Contains the overall state of the game
 * Created by Daryl on 11/3/2014.
 */
public class MenuModel {
    Map<UUID, RestGameModel> runningGames = new HashMap<UUID, RestGameModel>();
    static MenuModel instance = null;
    //singleton getinstance
    static MenuModel getInstance()
    {
        if(instance == null)
            instance = new MenuModel();
        return instance;
    }

    //empty constructor
    private  MenuModel(){}

    public Set<UUID> getIdentifiers()
    {
        return runningGames.keySet();
    }

    public RestGameModel getGame(UUID indentifier)
    {
        return runningGames.get(indentifier);
    }

    public void addGame(RestGameModel game)
    {
        UUID identifier = UUID.fromString(game.gameId);
        this.runningGames.put(identifier,game);
    }

    //modifies an xisting
    public void modifyGame(GameModel game, UUID identifier)
    {
        game.identifier = identifier;
       // this.runningGames.put(identifier,game);
    }

    public void removeGame(UUID indentifier)
    {
        runningGames.remove(indentifier);
    }

    public void reset() {
        this.runningGames = new HashMap<UUID, RestGameModel>();
    }


    /** ALL THE REST STUFF **/
    public void refreshGameList()
    {
      //  getGameList();
    }


}
