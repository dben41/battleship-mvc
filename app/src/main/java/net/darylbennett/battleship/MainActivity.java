package net.darylbennett.battleship;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * Created by Daryl on 11/3/2014.
 */
public class MainActivity extends Activity{

    GameModel currentGame;
    GameListFragment gameListFragment;
    GameFragment gameFragment;
    private Timer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //test all the REST crap


        //MenuModel.getInstance().getGameDetails("414f483a-7031-4c2d-8473-42e7aef39f30");
        //MenuModel.getInstance().getGameList();
        //MenuModel.getInstance().joinGame("756dfd57-e109-4658-b24d-cafbee9e9bda","Derylr");
        //MenuModel.getInstance().FireMissle("756dfd57-e109-4658-b24d-cafbee9e9bda", "5321c098-ed3a-4c53-8bb6-2facb6d74bc3",1,1);
        //MenuModel.getInstance().WhoseTurn("756dfd57-e109-4658-b24d-cafbee9e9bda", "5321c098-ed3a-4c53-8bb6-2facb6d74bc3");
        //MenuModel.getInstance().WhoseTurn("756dfd57-e109-4658-b24d-cafbee9e9bda", "39ded2af-dea1-4030-9c5f-88460438c8c0");
        //MenuModel.getInstance().GetBoard("756dfd57-e109-4658-b24d-cafbee9e9bda", "39ded2af-dea1-4030-9c5f-88460438c8c0");

       // MenuModel.getInstance().refreshGameList();
        //timer crap



        LinearLayout rootLayout = new LinearLayout(this);
        rootLayout.setOrientation(LinearLayout.HORIZONTAL);

        FrameLayout artListLayout = new FrameLayout(this);
        artListLayout.setId(10);
        rootLayout.addView(artListLayout, new LinearLayout.LayoutParams(
                0, ViewGroup.LayoutParams.MATCH_PARENT,20
        ));

        FrameLayout gameLayout = new FrameLayout(this);
        gameLayout.setId(11);
        rootLayout.addView(gameLayout, new LinearLayout.LayoutParams(
                0, ViewGroup.LayoutParams.MATCH_PARENT,80
        ));

        setContentView(rootLayout);

        //declare the fragments

        gameListFragment = new GameListFragment();

        gameFragment = new GameFragment();

        FragmentTransaction addTransaction = getFragmentManager().beginTransaction();
        addTransaction.add(10,gameListFragment);
        addTransaction.add(11, gameFragment);
        addTransaction.commit();


        this.getGameList();

        gameListFragment.setOnGameSelectedListener(new GameListFragment.OnGameSelectedListener() {
            @Override
            public void onGameSelected(GameListFragment gameListFragment, UUID identifier) {
                gameListFragment.setCurrentGame(identifier);
                //I don't want to join my own games
                RestGameModel rgm = MenuModel.getInstance().getGame(identifier);
                //if(rgm.playerId == null)
                    joinGame(identifier.toString(),"Player 2");
//                else
//                    warningMessage();
                //set the game fragment
                gameListFragment.gameListView.invalidateViews(); //i could maybe pass in current game?


            }
        });


//        gameFragment.setMissleLaunchedListener(new GameFragment.MissleLaunchListener() {
//            @Override
//            public void missleLaunched() {
//                gameListFragment.gameListView.invalidateViews();
//            }
//        });

        gameListFragment.setNewGameListener(new GameListFragment.NewGameListener() {
            @Override
            public void addNewGameEvent() {
                createGame("Match Daryl", "Player 1");
            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();

//        try {
//            File file = new File(getFilesDir(), "runningGames.txt");
//            FileWriter write = new FileWriter(file);
//            BufferedWriter bufferedTextWriter = new BufferedWriter(write);
//            Gson gson = new Gson();
//            //write different things
//            String gsonText = gson.toJson(MenuModel.getInstance());
//            //get the identifiers of running games
//           // MenuModel.getInstance().getIdentifiers();
//            //iterate through each of the identifiers,
//            //and map them to their games
//
//            String test = "";
//            bufferedTextWriter.write(gsonText);
//            bufferedTextWriter.close();
//
//        } catch (IOException e){
//            e.printStackTrace();
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        try{
//        File file = new File(getFilesDir(), "paletteState.txt");
//        FileReader textReader = new FileReader(file);
//        BufferedReader bufferedReader = new BufferedReader(textReader);
//        String paletteList = null;
//        paletteList = bufferedReader.readLine();
//
//        Gson gson = new Gson();

         //gson.fromJson(MenuModel.getInstance(), MenuModel.class);
        //Integer[] colors = gson.fromJson(paletteList, Integer[].class);
        //get stuff

//
//    }catch(IOException e){
//        e.printStackTrace();
//    } catch(Exception e)
//    {
//        e.printStackTrace();
//    }

    }


    /*******
     *  New Handler methods
     */
    public void getGameList() {
        AsyncTask<String, Integer, RestInterface.GameListItem[]> getGameListTask = new AsyncTask<String, Integer, RestInterface.GameListItem[]>() {
            @Override
            protected RestInterface.GameListItem[] doInBackground(String... params) {

                return RestInterface.getGameList();
            }

            @Override
            protected void onPostExecute(RestInterface.GameListItem[] gameListItems) {
                super.onPostExecute(gameListItems);
                ArrayList<RestInterface.GameDetails> gameDetails = new ArrayList<RestInterface.GameDetails>();
                //empties out the list
                //MenuModel.getInstance().reset();
                for(RestInterface.GameListItem gameListItem: gameListItems)
                {
                    //check to see if your games are in the collection
                    UUID tempId = UUID.fromString(gameListItem.id);
                    if(MenuModel.getInstance().getGame(tempId) == null || MenuModel.getInstance().getGame(tempId).playerId == null) {

                        RestGameModel rgm = new RestGameModel();
                        //get the fields from the GameListItem
                        rgm.gameName = gameListItem.name;
                        rgm.gameId = gameListItem.id;
                        rgm.gameStatus = gameListItem.status;

                        //get the details of the game
                        //how exactly do i get the id's?
                        //check to see if I own any of these games, add up the
                        //TODO add other info if i can, playerId, yourTurn, gameBoards

                        //experiment 1, only add games that are waiting
                        if (rgm.gameStatus == RestInterface.GameStatus.WAITING)
                            MenuModel.getInstance().addGame(rgm);
                    }



                    //gameDetails.add(getGameDetails(gameListItem.id)); //test code
                }
                if(timer==null) {
                    timer = new Timer();
                    timer.scheduleAtFixedRate(new RefreshTask(), 0, 2100);
                }
                gameListHandler.sendEmptyMessage(0);
                MenuModel.getInstance();
                return;
            }

        };
        getGameListTask.execute();
    }

    class RefreshTask extends TimerTask {
        @Override
        public void run() {

            //error check
            if(gameListFragment == null || gameListFragment.gameListView == null)
                return;
            if(gameListFragment.currentGame != null)
            {
                RestGameModel rgm = MenuModel.getInstance().getGame(gameListFragment.currentGame);
                if(rgm.gameId != null && rgm.playerId != null && rgm.playerGrid != null && rgm.opponentGrid != null) {
                    RestInterface.Board b = GetBoard(rgm.gameId, rgm.playerId); //might be better in run?
                    rgm.playerGrid = transformGrid(b.playerBoard);
                    rgm.opponentGrid = transformGrid(b.opponentBoard);
                    MenuModel.getInstance().addGame(rgm);
                }
            }
            getGameList();
            SpecialGameHandler.sendEmptyMessage(0);
            //gameListFragment.gameListView.invalidateViews();
        }
    }

    public void createGame(final String gameName, final String playerName) {
        AsyncTask<String, Integer, RestInterface.NewGameInfo> createGameTask = new AsyncTask<String, Integer, RestInterface.NewGameInfo>() {
            @Override
            protected RestInterface.NewGameInfo doInBackground(String... params) {

                return RestInterface.createGame(gameName,playerName);
            }

            @Override
            protected void onPostExecute(RestInterface.NewGameInfo newGameInfo) {
                super.onPostExecute(newGameInfo);
                UUID uuid = UUID.fromString(newGameInfo.gameId);
                RestGameModel rgm = new RestGameModel();//this is null
                rgm.gameName = gameName;
                rgm.playerName = "Player 1";
                //if just created, then must be waiting
                rgm.gameStatus = RestInterface.GameStatus.WAITING;
                //default missle launch
                rgm.misslesLaunched = 0;
                rgm.playerId = newGameInfo.playerId;
                rgm.gameId = newGameInfo.gameId;
                //game not in play, and therefore doesn't need to be called yet.
                MenuModel.getInstance().addGame(rgm);
                //rgm.board1 = GetBoard(newGameInfo.gameId, newGameInfo.playerId);

                gameListHandler.sendEmptyMessage(0);
            }
        };

        createGameTask.execute();

    }

    public RestInterface.GameDetails getGameDetails(final String gameId) {
        AsyncTask<String, Integer, RestInterface.GameDetails> createGameTask = new AsyncTask<String, Integer, RestInterface.GameDetails>() {
            @Override
            protected RestInterface.GameDetails doInBackground(String... params) {

                return RestInterface.getGameDetails(gameId);
            }
        };

        try {
            return createGameTask.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        //didn't find it
        return null;
    }

    public void joinGame(final String gameId, final String playerName) {
        AsyncTask<String, Integer, String> joinGameTask = new AsyncTask<String, Integer, String>() {
            @Override
            protected String doInBackground(String... params) {

                return RestInterface.joinGame(playerName,gameId);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //error check
                final UUID uuid = UUID.fromString(gameId);
                if(!(s.equals("Game could not be joined."))) {

                    RestGameModel tempRgm = MenuModel.getInstance().getGame(uuid);
                    tempRgm.playerId = s;
                    tempRgm.gameStatus = RestInterface.GameStatus.PLAYING;
                    //get the board?

                    RestInterface.Board board1 = GetBoard(gameId, s);
                    tempRgm.playerGrid = transformGrid(board1.playerBoard);
                    tempRgm.opponentGrid = transformGrid(board1.opponentBoard);
                    //get whose turn it is?
                    //if I'm joining the game, should I just set the current Fragment to this?


                    //add to the collection
                    MenuModel.getInstance().addGame(tempRgm);
                } else{
                    //This is if it's your game.
                    RestGameModel tempRgm = MenuModel.getInstance().getGame(uuid);
                    if(tempRgm.playerId != null)
                    {
                        tempRgm.gameStatus = RestInterface.GameStatus.PLAYING;
                        RestInterface.Board board1 = GetBoard(tempRgm.gameId, tempRgm.playerId);
                        tempRgm.playerGrid = transformGrid(board1.playerBoard);
                        tempRgm.opponentGrid = transformGrid(board1.opponentBoard);
                        MenuModel.getInstance().addGame(tempRgm);
                    }
                }

                gameFragment.setGameDetailIdentifier(uuid);
                //do some setup stuff here ************************************
                gameFragment.playerTwoView.setOnGridTouchListener(new TestGridView.OnGridTouchListener() {
                    @Override
                    public void onGridTouch(int x, int y) {
                        String playerId = MenuModel.getInstance().getGame(uuid).playerId;
                        RestInterface.WhoseTurnIsItObject wto= WhoseTurn(gameId, playerId);
                        //TODO: ask if game is over
                        if(!wto.isYourTurn)
                        {
                            if(!wto.winner.equals("IN PROGRESS")) {
                                winnerDeclared(wto.winner);
                                RestGameModel tempRgm = MenuModel.getInstance().getGame(uuid);
                                tempRgm.gameStatus = RestInterface.GameStatus.DONE;
                            }
                            return;
                        }
                        if(!wto.winner.equals("IN PROGRESS"))
                        {
                            //pop-up window
                            winnerDeclared(wto.winner);
                            RestGameModel tempRgm = MenuModel.getInstance().getGame(uuid);
                            tempRgm.gameStatus = RestInterface.GameStatus.DONE;
                            //mark game as over
                            MenuModel.getInstance().addGame(tempRgm);
                            gameHandler.sendEmptyMessage(0);

                            return;
                        }
                        RestInterface.GuessResult fireResult = FireMissle(gameId,playerId,x,y);
                        //TODO do something with ships sunk
                        //if(fireResult.shipsSunk == 5) //YOU WIN! Game over
                        RestGameModel tempRgm = MenuModel.getInstance().getGame(uuid);
                        RestInterface.Board board1 = GetBoard(gameId, tempRgm.playerId);
                        tempRgm.playerGrid = transformGrid(board1.playerBoard);
                        tempRgm.opponentGrid = transformGrid(board1.opponentBoard);
                        RestInterface.GameDetails gd= getGameDetails(tempRgm.gameId);
                        tempRgm.misslesLaunched = gd.missilesLaunched; //verify this works, not very performant. I like +=2 better
                        MenuModel.getInstance().addGame(tempRgm);
                        gameFragment.playerOneView.grid = transformGrid(board1.playerBoard);
                        gameFragment.playerTwoView.grid = transformGrid(board1.opponentBoard);
                        gameFragment.textView.setText("GAME: " + gd.name + " | " + gd.player1 + " vs. " + gd.player2 );


                    }
                });
                gameHandler.sendEmptyMessage(0);

            }
        };
        joinGameTask.execute();
    }

    private Point.Status[][] transformGrid(RestInterface.BoardSquare[] bs) {
        Point.Status[][] ps = new Point.Status[10][10];
        for(RestInterface.BoardSquare b: bs){
            switch (b.status){
                case HIT:
                    ps[b.xPos][b.yPos] = Point.Status.HIT;
                    break;
                case MISS:
                    ps[b.xPos][b.yPos] = Point.Status.MISS;
                    break;
                case SHIP:
                    ps[b.xPos][b.yPos] = Point.Status.SHIP;
                    break;
                case NONE:
                    ps[b.xPos][b.yPos] = Point.Status.OCEAN;
                    break;
                default:
                    break;
            }
        }
        return ps;
    }

    public RestInterface.GuessResult FireMissle(final String gameId, final String playerId, final int xPos, final int yPos) {
        AsyncTask<String, Integer, RestInterface.GuessResult> joinGameTask = new AsyncTask<String, Integer, RestInterface.GuessResult>() {
            @Override
            protected RestInterface.GuessResult doInBackground(String... params) {

                return RestInterface.fireMissle(playerId,xPos,yPos,gameId);
            }
        };
        try {
            gameHandler.sendEmptyMessage(0);
            return joinGameTask.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public RestInterface.WhoseTurnIsItObject WhoseTurn(final String gameId, final String playerId) {
        AsyncTask<String, Integer, RestInterface.WhoseTurnIsItObject> whoseTurnTask = new AsyncTask<String, Integer, RestInterface.WhoseTurnIsItObject>() {
            @Override
            protected RestInterface.WhoseTurnIsItObject doInBackground(String... params) {

                return RestInterface.whosTurn(gameId, playerId);
            }
        };
        try {
           return whoseTurnTask.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public RestInterface.Board GetBoard(final String gameId, final String playerId) {
        AsyncTask<String, Integer, RestInterface.Board> whoseTurnTask = new AsyncTask<String, Integer, RestInterface.Board>() {
            @Override
            protected RestInterface.Board doInBackground(String... params) {

                return RestInterface.getBoard(gameId, playerId);
            }
        };
        try {
            return whoseTurnTask.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }





    /**
     * ALl the handler crap
     */
    public final Handler gameListHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //super.handleMessage(msg);
            gameListFragment.gameListView.invalidateViews();
        }
    };

    //invalidates if missle fired
    public final Handler gameHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //super.handleMessage(msg);
            gameFragment.playerOneView.invalidate();
            gameFragment.playerTwoView.invalidate();
            gameFragment.textView.invalidate();
            gameListFragment.gameListView.invalidateViews();
        }
    };
    int updateCount = 7;
    public final Handler SpecialGameHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            //super.handleMessage(msg);
            if(gameListFragment != null && gameListFragment.gameListView != null)
           {
                 //   getGameList();
                gameListFragment.gameListView.invalidateViews();
           }
            if(gameFragment != null && gameFragment.playerOneView != null && gameFragment.playerTwoView != null && gameListFragment.currentGame != null)
            {
                //get board
                RestGameModel rgm = MenuModel.getInstance().getGame(gameListFragment.currentGame);
                //update the views
                gameFragment.playerOneView.grid = rgm.playerGrid;
                gameFragment.playerTwoView.grid = rgm.opponentGrid;
                gameFragment.playerOneView.invalidate();
                gameFragment.playerTwoView.invalidate();
            }
        }
    };


    public void winnerDeclared(String winner)
    {
        // Show popup dialog box
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Game Over.")
                .setMessage(winner + " has won the game! Thank you for playing. " )
                .setNeutralButton("Ready", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        gameFragment.playerOneView.setVisibility(View.VISIBLE);
                        gameFragment.playerTwoView.setVisibility(View.VISIBLE);
                    }

                })
                .show();
    }

    public void warningMessage()
    {
        // Show popup dialog box
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Error.")
                .setMessage("You can't play yourself! Please select another game." )
                .setNeutralButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        gameFragment.playerOneView.setVisibility(View.VISIBLE);
                        gameFragment.playerTwoView.setVisibility(View.VISIBLE);
                    }

                })
                .show();
    }



}
