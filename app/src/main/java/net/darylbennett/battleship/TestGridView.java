package net.darylbennett.battleship;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Daryl on 11/2/2014.
 */
public class TestGridView extends View {

    int width;
    int height;
    int radius;
    Point.Status[][] grid;
    boolean cloakGrid;
    boolean disable;




    public TestGridView(Context context) {
        super(context);
        width = getWidth();
        height = getHeight();
        radius = (int)(width * 0.08)/2;
        //set to true when game is over
        disable = false;
        //we need to set the player1Grid
    }

    public void updateViews(Point.Status[][] grid)
    {
        this.grid = grid;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        circlePaint.setColor(Color.BLUE);

        //create border
        //borrowed from http://androidcodeninja.blogspot.com/2013/01/android-add-border-to-relativelayout.html
        Paint linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        linePaint.setStrokeWidth(5);
        linePaint.setColor(Color.BLACK);
        linePaint.setStyle(Paint.Style.STROKE);
        canvas.drawLine(0,0, getWidth(), 0, linePaint);
        canvas.drawLine(0,0, 0, getHeight(), linePaint);
        canvas.drawLine(getWidth(),0, getWidth(),getHeight(),linePaint);
        canvas.drawLine(0,getHeight(), getWidth(),getHeight(),linePaint);
        setBackgroundColor(Color.CYAN);

        width = getWidth();
        height = getHeight();
        radius = (int)(width * 0.09)/2;
        if(width> height)
            radius = (int)(height * 0.09)/2;
        int y, x = 0;
        for(double j = 0.05; j<=1.0; j+=0.1) {
            for (double i = 0.05; i <= 1.0; i += 0.1) {
                y = (int)((j+0.04)*10);
                x = (int)((i+0.04)*10);
                if(grid != null)
                {
                    if((grid[x][y] == Point.Status.OCEAN))
                        circlePaint.setColor(Color.BLUE);
                    else if((grid[x][y] == Point.Status.SHIP)&& cloakGrid==false)
                        circlePaint.setColor(Color.DKGRAY);
                    else if(grid[x][y] == Point.Status.HIT) {
                        circlePaint.setColor(Color.RED);
//                        Log.i("HIT", "These circles have been hit: x:" + y + " ,y: " + x);
//                        Log.i("HIT", "These circles have been hit: x:" + j + " ,y: " + i);
                    }
                    else if(grid[x][y] == Point.Status.MISS) {
                        circlePaint.setColor(Color.WHITE);
//                        Log.i("HIT", "These calculated circles have been missed: x:" + y + " ,y: " + x);
//                        Log.i("HIT", "These exact circles have been missed: x:" + j + " ,y: " + i);
                    }
                }
                canvas.drawCircle((int) (width * i), (int) (height * j), radius, circlePaint);
                circlePaint.setColor(Color.BLUE);
            }
        }

    }

    //usually part of initialzing this
    public void setGrid(Point.Status[][] grid) {
        grid = grid;
    }

    //Listener stuff
    public interface OnGridTouchListener{
        public void onGridTouch(int x, int y);
    }
    OnGridTouchListener onGridTouchListener = null;
    public void setOnGridTouchListener(OnGridTouchListener listener){
        onGridTouchListener = listener;
    }

    public void disable()
    {
        disable = true;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(disable)
        {
            warningWindow();
            return false;
        }
        float x = event.getX();
        float y = event.getY();
        float coordinateX = (x/getWidth())*10;
        float coordinateY = (y/getHeight())*10;
        //determine what cell was pressed in the grid
        //Log.i("TestGridView", "I touched this: x:" + (int)coordinateX + " ,y: " + (int)coordinateY);
        if(onGridTouchListener != null)
             onGridTouchListener.onGridTouch((int)coordinateX,(int)coordinateY);
        else {
           popupWindow();
        }
        return super.onTouchEvent(event);

    }
    public void popupWindow()
    {

        // Show popup dialog box
        new AlertDialog.Builder(getContext())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Warning")
                .setMessage("Please select a game on the left. \n If no game is available, please click on the 'Add' Button." )
                .setNeutralButton("Ready", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }

                })
                .show();
    }

    public void warningWindow()
    {

        // Show popup dialog box
        new AlertDialog.Builder(getContext())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Warning")
                .setMessage("The current game is over. Please select or add a new game on the left." )
                .setNeutralButton("Ready", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }

                })
                .show();
    }
    //Model Listener
}
